package eu.larkc.csparql.readytogopack;
/*
 * @(#)CounterFormatter.java   1.0   01/ott/2009
 *
 * Copyright 2009-2009 Politecnico di Milano. All Rights Reserved.
 *
 * This software is the proprietary information of Politecnico di Milano.
 * Use is subject to license terms.
 *
 * @(#) $Id$
 */

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.reasoner.rulesys.impl.RETEEngine;
import com.hp.hpl.jena.shared.PrefixMapping;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.vocabulary.ReasonerVocabulary;
import eu.larkc.csparql.common.RDFTable;
import eu.larkc.csparql.common.RDFTuple;
import eu.larkc.csparql.common.streams.format.GenericObservable;
import eu.larkc.csparql.core.ResultFormatter;
import ie.tcd.cs.nembes.coror.Coror;
import ie.tcd.cs.nembes.coror.debug.Debugger;
import ie.tcd.cs.nembes.coror.graph.Factory;
import ie.tcd.cs.nembes.coror.graph.Node;
import ie.tcd.cs.nembes.coror.graph.Triple;
import ie.tcd.cs.nembes.coror.graph.temporal.TemporalTriple;
import ie.tcd.cs.nembes.coror.rdf.model.GraphWriter;
import ie.tcd.cs.nembes.coror.reasoner.FGraph;
import ie.tcd.cs.nembes.coror.reasoner.InfGraph;
import ie.tcd.cs.nembes.coror.reasoner.rulesys.RETERuleInfGraph;
import ie.tcd.cs.nembes.coror.util.iterator.ExtendedIterator;
import java.io.BufferedWriter;
import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class newConsoleFormatter extends ResultFormatter {

    Coror coror = new Coror("../Coror/resources/reasoner.config");
    File file1;
    FileWriter fos1;
    File file2;
    FileWriter fos2;
    File file3;
    FileWriter fos3;
    File file4;
    FileWriter fos4;
    File file5;
    FileWriter fos5;
    File file6;
    FileWriter fos6;
    File file7;
    FileWriter fos7;
    BufferedWriter bw1;
    BufferedWriter bw2;
    BufferedWriter bw3;
    BufferedWriter bw4;
    BufferedWriter bw5;
    BufferedWriter bw6;
    BufferedWriter bw7;
    int i;
    FGraph prevAdd;
    FGraph tempprevAdd;
    Node subj;
    Node pred;
    Node obj;
    Triple trip;
    InfGraph infGraph;
    ie.tcd.cs.nembes.coror.graph.Graph schemaGraph;
    ie.tcd.cs.nembes.coror.graph.Graph Graph = Factory.createDefaultGraph();
    private GraphWriter graphWriter;
    private int range = 1;


    @Override
    public void update(final GenericObservable<RDFTable> observed, final RDFTable q) {
        long updateStartTime = System.currentTimeMillis();
        if (i == 0) {
            file1 = new File("/home/joren/Documents/Stages/tcd/Coror/resources/Results/tiw.txt");
            file2 = new File("/home/joren/Documents/Stages/tcd/Coror/resources/Results/tba.txt");
            file3 = new File("/home/joren/Documents/Stages/tcd/Coror/resources/Results/ded.txt");
            file4 = new File("/home/joren/Documents/Stages/tcd/Coror/resources/Results/time.txt");
            // file5 = new File("/home/joren/Documents/Stages/tcd/Coror/resources/Results/adds.txt");
            file6 = new File("/home/joren/Documents/Stages/tcd/Coror/resources/Results/dels.txt");
            file7 = new File("/home/joren/Documents/Stages/tcd/Coror/resources/Results/mem.txt");

            try {
                fos1 = new FileWriter(file1);
                bw1 = new BufferedWriter(fos1);
                fos2 = new FileWriter(file2);
                bw2 = new BufferedWriter(fos2);
                fos3 = new FileWriter(file3);
                bw3 = new BufferedWriter(fos3);
                fos4 = new FileWriter(file4);
                bw4 = new BufferedWriter(fos4);
                //fos5 = new FileWriter(file5);
                //bw5 = new BufferedWriter(fos5);
                fos6 = new FileWriter(file6);
                bw6 = new BufferedWriter(fos6);
                fos7 = new FileWriter(file7);
                bw7 = new BufferedWriter(fos7);
            } catch (IOException ex) {
                Logger.getLogger(newConsoleFormatter.class.getName()).log(Level.SEVERE, null, ex);
            }


            //load ontology and rules
            System.out.println("initialization");
            coror.loadOntology();
            coror.loadRules();
            //coror.setSchema();
            //
            coror.startReasoner();
            infGraph = coror.getInfGraph();
        }



        long startTime = System.currentTimeMillis();



        ie.tcd.cs.nembes.coror.graph.Graph ontGraph = coror.getOntGraph();


        System.out.println(q.size() + " Triples at time " + i);
        try {

            bw1.write(Integer.toString(q.size()));
            bw1.newLine();
            bw1.flush();

        } catch (IOException ex) {
            Logger.getLogger(newConsoleFormatter.class.getName()).log(Level.SEVERE, null, ex);
        }
        long addstart = System.currentTimeMillis();
        int k = 0;
        /**
         * clear/create additions graph*
         */
        ((RETERuleInfGraph) infGraph).fadd = new FGraph(Factory.createDefaultGraph());

        //this is the loop where triples are added from window to a graph in reasoner
        for (final RDFTuple t : q) {

            //System.out.println(t.toString());
            //Resource createOntResource = ontModel.createResource(t.get(0));
            //System.out.println(t.get(0));
            subj = Node.create(t.get(0));
            //Property createProperty = ontModel.createProperty(t.get(1));
            //System.out.println(t.get(1));
            pred = Node.create(t.get(1));
            //Literal createLiteral = ontModel.createLiteral(t.get(2));
            //System.out.println(t.get(2));
            obj = Node.create(t.get(2));
            trip = new TemporalTriple(subj, pred, obj, i + 2);
            //ontModel.addLiteral(createOntResource, createProperty, createLiteral);
            //System.out.println("this is added "+trip.toString());
            long adds = System.currentTimeMillis();
            /**
             * adds these triples to the separate additions graph*
             */
            ((RETERuleInfGraph) infGraph).addnewTriple(trip);

            long adde = System.currentTimeMillis();
            //System.out.println("add"+k+" "+(adde-adds));
            //System.out.println(ontModel.size());
            k++;
        }
        /*
         ExtendedIterator find2 = ((RETERuleInfGraph)infGraph).fadd.getGraph().find(Node.ANY, Node.ANY, Node.ANY);
         System.out.println("All things in fadd b4");
         while(find2.hasNext()){
         System.out.println(find2.next().toString());
         }
         */


        //System.out.println("size to be added "+((RETERuleInfGraph)infGraph).fadd.getGraph().size());
        try {
            bw2.write(Integer.toString(((RETERuleInfGraph) infGraph).fadd.getGraph().size()));
            bw2.newLine();
            bw2.flush();
        } catch (IOException ex) {
            Logger.getLogger(newConsoleFormatter.class.getName()).log(Level.SEVERE, null, ex);
        }
        //((RETERuleInfGraph)coror.getInfGraph()).runAll();
        long addend = System.currentTimeMillis();
        //System.out.println("add time "+(addend-addstart));
        //System.out.println("Sweeping Graph");
        infGraph.prepare();
        // System.out.println(infGraph.size());


        ExtendedIterator it = infGraph.find(Node.ANY, Node.ANY, Node.ANY);


        long sweepStart = System.currentTimeMillis();
        int z = 0;
        while (it.hasNext()) {
            Object next = it.next();
            //System.out.println(next.toString());
            if (next instanceof TemporalTriple) {
                TemporalTriple temp = (TemporalTriple) next;


                if (temp.getTime() < Math.max(0,i - (range-1))) {
                    z++;
                }
                //expired triples deleted from ontology here
                ((RETERuleInfGraph) infGraph).performDeleteTimeTriples(temp, Math.max(0,i - (range-1)));



            }
        }
//System.out.println(z+" deletes");
        try {
            bw6.write(Integer.toString(z));
            bw6.newLine();
            bw6.flush();
        } catch (IOException ex) {
            Logger.getLogger(newConsoleFormatter.class.getName()).log(Level.SEVERE, null, ex);
        }
        long startReteTime = System.currentTimeMillis();
        //System.out.println("Sweeping RETE "+i);
//RETE network sweeped here, expired tokens removed
        ((RETERuleInfGraph) infGraph).sweepRete(Math.max(0,i - (range-1)));
        long endReteTime = System.currentTimeMillis();
        // System.out.println("RETE time"+(endReteTime-startReteTime));
        long endSweepTime = System.currentTimeMillis();

//System.out.println("inf graph size "+infGraph.size());
//System.out.println("Rebinding");
        long startRebindTime = System.currentTimeMillis();
        infGraph.rebind();
        infGraph.prepare();
        long endTime = System.currentTimeMillis();

//System.out.println("Total re-reasoning time"+(endTime-startTime));
        try {
            bw4.write(Long.toString(endTime - startTime));
            bw4.newLine();
            bw4.flush();
        } catch (IOException ex) {
            Logger.getLogger(newConsoleFormatter.class.getName()).log(Level.SEVERE, null, ex);
        }
//System.out.println("mem used "+Debugger.getMemUsage());
        try {
            bw7.write(Long.toString(Debugger.getMemUsage()));
            bw7.newLine();
            bw7.flush();
        } catch (IOException ex) {
            Logger.getLogger(newConsoleFormatter.class.getName()).log(Level.SEVERE, null, ex);
        }
//System.out.println("ded size "+infGraph.getDeductionsGraph().size());
        try {
            bw3.write(Integer.toString(infGraph.getDeductionsGraph().size()));
            bw3.newLine();
            bw3.flush();
        } catch (IOException ex) {
            Logger.getLogger(newConsoleFormatter.class.getName()).log(Level.SEVERE, null, ex);
        }

        i++;

        System.out.println("raw graph (" + infGraph.getRawGraph().size() + ")" );
        //ExtendedIterator findraw = infGraph.getRawGraph().find(Node.ANY, Node.ANY, Node.ANY);
//        while (findraw.hasNext()) {
//            Object obj = findraw.next();
//            if (obj instanceof TemporalTriple) {
//                System.out.println(obj.toString());
//            }
//        }
        
        System.out.println("deductions  (" + infGraph.getDeductionsGraph().size() + ")");
        ExtendedIterator find = infGraph.getDeductionsGraph().find(Node.ANY, Node.ANY, Node.ANY);
//        while (find.hasNext()) {
//            Object obj = find.next();
//            if (obj instanceof TemporalTriple) {
//                System.out.println(obj.toString());
//            }
//        }
        
        
    }
}
